.. _config_settings:

WirePlumber Settings
====================

This section describes the settings that can be configured on WirePlumber.

Settings can be either configured statically in the configuration file
by setting them under the ``wireplumber.settings`` section, or they can be
configured dynamically at runtime by using metadata.

.. include:: ../../../../src/scripts/lib/SETTINGS.rst
